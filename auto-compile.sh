#!/bin/bash

g++ -c -O2 -Wall jetsonGPIO/jetsonGPIO.c -o jetsonGPIO/jetsonGPIO

mkdir build
cd build
cmake ..
make
# ./kinect-detection