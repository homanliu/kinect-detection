// Standard Library
#include <iostream>
#include <string>
#include <algorithm>
#include <math.h>
#include <stdlib.h>
#include <iomanip>
#include <signal.h>
#include <pthread.h>
#include <time.h>
#include <vector>

// Linux Header
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// OpenCV Header
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp> 

// Libfreenect2 Header
#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/registration.h>
#include <libfreenect2/packet_pipeline.h>
#include <libfreenect2/logger.h>

#include "goal-detection-linux.hpp"
#include "jetsonGPIO/jetsonGPIO.h"

using namespace std;
using namespace cv;

void sigint_handler(int s){
        protonect_shutdown = true;
}

void display_on_screen(){
    time_t currentTime = time(NULL);
    if (currentTime - lastUpdateTime <= 2){
        if(shootingResult == true){
            putText(imageDisplay, string("Goal"), Point(430, 30), 0, 1, Scalar(0, 127, 255), 2);
            gpioSetValue(goalSignal, on);
        }
        else{
            putText(imageDisplay, string("Fail"), Point(430, 30), 0, 1, Scalar(0, 127, 255), 2);
            gpioSetValue(goalSignal, off);
        }
    }
    else{
        gpioSetValue(goalSignal, off);
    }
}

// Calculate minimum meaningful colour range
void colorCalculation(int *lowerColor){
    // Pythagoras's theorem, a^2 + b^2 = c^2
    double centreToKinect = sqrt(pow(fenceHeight - kinectHeight, 2) + pow(fenceToKinect, 2));
    double lowerDistance = centreToKinect - tolerance;
    // Map from depth value to grayscale, change here if using raw 16 bits
    *lowerColor = int(8000.0 * lowerDistance/maxDepthRange);
}

int getRodCoordinates(int imageWidth, int **whitePoints, int pointCount){
    // Initialize array with all zero to store satisfied x-coordinates
    int *rowWhiteNumber = new int[imageWidth];
    int rowWhiteCount = 0, yCoordinates = 0;
    for(int i=0; i<10; i++)
        rowWhiteNumber[i] = -1;

    int currentX = 0, consecutiveCount = 0, lastHeight = 0;
    bool chosen = false, garbage = false;
    // Finding x-coordinate of inner circle in set of white points
    for(int i=0; i<pointCount; i++){
        // All points are already sorted according to x-coordinate
        if(whitePoints[0][i] < imageWidth / 2){
            currentX = 0;
            continue;
        }
        if(currentX != whitePoints[1][i]){
            // Re-count if x-coordinate changed
            currentX = whitePoints[1][i];
            lastHeight = whitePoints[0][i];
            consecutiveCount = 0;
            chosen = false;
            garbage = false;
        }
        else{
            // Give tolerance since poor accuracy for long distance
            if(whitePoints[0][i] - lastHeight <= 2 && !garbage){
                // Keep counting if we detected consecutive white
                consecutiveCount += 1;
                lastHeight = whitePoints[0][i];
            }
            // else
            //     garbage = true;
            // Large consecutive count means that it is probably part of rod
            if(consecutiveCount >= rodLength && !chosen){
                // Mark it and denoted as chosen to prevent re-adding
                rowWhiteNumber[rowWhiteCount] = currentX;
                rowWhiteCount += 1;
                chosen = true;
            }
        }
    }

    if(rowWhiteCount > 0){
        // New Algorithm: Find width of all objects in line and ignore unreasonable width
        int temp = 1;
        for(int i=temp; i<rowWhiteCount; i++){
            if(rowWhiteNumber[i] - rowWhiteNumber[i-1] != 1 || (i == rowWhiteCount - 1 && rowWhiteNumber[i] - rowWhiteNumber[i-1] == 1)){
                // Numbers represent pixels, so width has to +1
                int width = rowWhiteNumber[i-1] - rowWhiteNumber[temp-1] + 1;
                if(i == rowWhiteCount - 1 && rowWhiteNumber[i] - rowWhiteNumber[i-1] == 1)
                    width = rowWhiteNumber[i] - rowWhiteNumber[temp-1] + 1;
                if(width > 1 && width <= rodWidth){
                    // Simply take middle one in the consecutive numbers
                    yCoordinates = rowWhiteNumber[temp - 1 + int(width/2)];

                    // Free used objects to prevent overflow
                    delete[] rowWhiteNumber;
                    return yCoordinates;
                }
                else{
                    temp = i + 1;
                }
            }
        }
        // Free used objects to prevent overflow
        delete[] rowWhiteNumber;
        return -1;
    }
    else{
        delete[] rowWhiteNumber;
        return -1;
    }
}

int getCircleCoordinates(int imageHeight, int **whitePoints, int pointCount, int centreX){
    int listOfHeight = 0, countList = 0, centreY = 0, threshold = 15;
    bool blackZone = false;
    for(int i=imageHeight; i>=0; i--){
        int count = 0;
        for(int j=0; j<pointCount; j++){
            // Test every points if they are inside circle, Increase count
            int dY = pow((whitePoints[0][j] - i), 2);
            int dX = pow((whitePoints[1][j] - centreX), 2);
            // Circle that must have smaller radius than inner one
            if(dX + dY < pow(15, 2))
                count += 1;
        }
        // If we get enough amounts of count, it means that now it is looping in the black area
        if(countList > threshold)
            blackZone = true;
        // Once we get a circle with too many white points, then we stop looping
        if(count > threshold && blackZone)
            break;
        else if(count <= threshold){
            listOfHeight += i;
            countList += 1;
        }
    }
    // Take sum of average to get y-coordinate of centre
    if(countList > 0){
        centreY = int(listOfHeight / countList);
        return centreY;
    }
    else
        return -1;
}

int getCircleRadius(int **whitePoints, int pointCount, cv::Point centre){
    // Test for the maximum acceptable radius
    int largestRadius = 0, threshold = 15;
    // 50 can be other values which is sufficiently large enough
    for(int i=0; i<50; i++){
        int count = 0;
        for(int j=0; j<pointCount; j++){
            // Test every points if they are inside circle, Increase count
            int dY = pow((whitePoints[0][j] - centre.y), 2);
            int dX = pow((whitePoints[1][j] - centre.x), 2);
            if(dX + dY < pow(i, 2))
                count += 1;
        }
        // Once we get a circle with too many white points, then we stop looping
        if(count > threshold)
            break;
        // Keep storing largest radius value
        else if(count <= threshold && i > largestRadius){
            largestRadius = i;
        }
    }
    if(largestRadius > 0)
        return largestRadius;
    else
        return -1;
}

// Draw function for finding inner circle
void drawBoundingArea(cv::Mat rawImage, cv::Mat rodImage, int **whitePoints, int pointCount){
    // Centre of bounding circle (x, y)
    cv::Point centre;

    if (pointCount == 0) {
		cout << "Not found" << endl;
	}
    
    centre.x = getRodCoordinates(rodImage.rows, whitePoints, pointCount);
    if(centre.x == -1){
        cout << "Cannot locate rod" << endl;
    }

    // int rodDepth = 0, count = 0;
    // for (int i = 8; i >= 0; i--){
    //     int tempDepth = int(rodImage.at<IMAGE_FORMAT>(rodImage.rows/2 - i, centre.x));
    //     if (tempDepth > 0){
    //         rodDepth += tempDepth;
    //         count += 1;
    //     }
    // }
    // zPos = rodDepth / count;

    centre.y = getCircleCoordinates(rodImage.cols / 2, whitePoints, pointCount, centre.x);
    if(centre.y == -1){
        cout << "Cannot find centre" << endl;
    }

    int largestRadius = getCircleRadius(whitePoints, pointCount, centre);
	if (largestRadius == -1) {
		cout << "Cannot find radius" << endl;
	}
    
    if (largestRadius == -1) {
		cout << "Cannot find radius" << endl;
	}
    
    int depthPos = -1;
    // Locate z-Position for goal circle
    for(int j=0; j<pointCount; j++){
        // Test every points if they are on circle
        int dY = pow((whitePoints[0][j] - centre.y), 2);
        int dX = pow((whitePoints[1][j] - centre.x), 2);
        if(abs(dX + dY - pow(largestRadius, 2)) < 50){
            int tempPosition = int(rodImage.at<IMAGE_FORMAT>(whitePoints[0][j], whitePoints[1][j]));
            if(tempPosition > depthPos)
                depthPos = tempPosition;
        }
    }
   
    if(centre.x > 0 && centre.y > 0 && largestRadius > 0 && depthPos > 0)
        locationQueue->enqueue(centre, largestRadius, depthPos);
    outputCircle = locationQueue->medianDistance();

	// Draw circle in raw image instead of processed image
    if(outputCircle.centre.x > 0 && outputCircle.centre.y > 0 && outputCircle.maxRadius > 0 && outputCircle.depthPos > 0){
        circleExist = true;
        circle(imageDisplay, outputCircle.centre, outputCircle.maxRadius, CV_RGB(255, 255, 255), 2);
        zPos = outputCircle.depthPos;
    }
}

void preFiltering(cv::Mat rawImage, cv::Mat rodImage, int lowerColorRange){
    int *whitePoints[2], pointCount = 0;
    // Reset to -1 since it will be updated each frame
    outputCircle.centre = cv::Point(-1, -1);
    outputCircle.maxRadius = -1;
    // whitePoints[0] = set of y-coordinates
    whitePoints[0] = new int[100000];
    // whitePoints[1] = set of x-coordinates
    whitePoints[1] = new int[100000];
    // Filter out unrelated pixels, change here if using raw 16 bits
    for(int j=0; j<rodImage.cols; j++){
        for(int i=0; i<rodImage.rows; i++){
            // Assume that the circle must be higher than image centre
            if(i >= rodImage.cols/2)
                rodImage.at<IMAGE_FORMAT>(i, j) = 0;
            // Trim out leftmost and rightmost 1/8 image to reduce noise
            else if(j <= rodImage.rows/8)
                rodImage.at<IMAGE_FORMAT>(i, j) = 0;
            else if(j >= rodImage.rows* 7/8)
                rodImage.at<IMAGE_FORMAT>(i, j) = 0;
            // Set all smaller than minimum colour value points to zero
            else if(rodImage.at<IMAGE_FORMAT>(i, j) <= lowerColorRange)
                rodImage.at<IMAGE_FORMAT>(i, j) = 0;
            else{
                // Set it to white and add to array for faster calculation
                if(pointCount < 100000){
                whitePoints[0][pointCount] = i;
                whitePoints[1][pointCount] = j;
                pointCount += 1;
            }
        }
    }
    }
    // Keep processing if there is at least one point
    if(pointCount > 0)
        drawBoundingArea(rawImage, rodImage, whitePoints, pointCount);
    delete[] whitePoints[0];
    delete[] whitePoints[1];
}

cv::Point3d circle_line_intersection(int circle_radius, cv::Point circle_centre, cv::Point3i point2, cv::Point3i point1, bool return_point1){
    // Find intersection points for line and circle
    // Reference: https://math.stackexchange.com/questions/228841/how-do-i-calculate-the-intersections-of-a-straight-line-and-a-circle
    cv::Point3d output_point = cv::Point3d(-1.0, -1.0, -1.0);

    double m, c, p, q, r, A, B, C, delta;
    p = circle_centre.x;
    q = circle_centre.y;
    r = circle_radius;
    if (point2.x - point1.x == 0)
        return output_point;
    // y = mx + c
    m = (point2.y - point1.y) / (point2.x - point1.x);
    c = point1.y - m * point1.x;

    A = 1 + pow(m, 2);
    B = 2.0 * (m * c - m * q - p);
    C = pow(q, 2) - pow(r, 2) + pow(p, 2) - (2.0 * c * q) + pow(c, 2);

    delta = pow(B, 2) - 4.0 * A * C;

    if (delta >= 0){
        cv::Point3d intersect_point1, intersect_point2;

        intersect_point1.x = (-B + sqrt(delta)) / (2 * A);
        intersect_point2.x = (-B - sqrt(delta)) / (2 * A);

        intersect_point1.y = m * intersect_point1.x + c;
        intersect_point2.y = m * intersect_point2.x + c;

        // Reference: https://math.stackexchange.com/questions/35857/two-point-line-form-in-3d
        double xz_slope = double(point2.z - point1.z) / (point2.x - point1.x);
        intersect_point1.z = xz_slope * (intersect_point1.x - point1.x) + point1.z;
        intersect_point2.z = xz_slope * (intersect_point2.x - point1.x) + point1.z;

        if(return_point1 == true)
            return intersect_point1;
        else
            return intersect_point2;
    }
    return output_point;
}

bool point_in_line_segment(cv::Point3d point, cv::Point3i line_point1, cv::Point3i line_point2){
    // Check whether input point lies in between line segment
    // Reference: https://stackoverflow.com/questions/328107/how-can-you-determine-a-point-is-between-two-other-points-on-a-line-segment
    double threshold = 0.3;
    double line_distance = sqrt(pow(line_point2.x - line_point1.x, 2) + pow(line_point2.y - line_point1.y, 2));

    double distance_with_point1 = sqrt(pow(point.x - line_point1.x, 2) + pow(point.y - line_point1.y, 2));
    double distance_with_point2 = sqrt(pow(point.x - line_point2.x, 2) + pow(point.y - line_point2.y, 2));

    if (distance_with_point1 + distance_with_point2 - line_distance < threshold)
        return true;
    else
        return false;
}

void goalDetection(){
    bool result = false;
    int pointCount = 0;
    intersectionPoint *pointsOnLine = new intersectionPoint[10];
    // Case 1: Get two intersection points for path
    for(int i=1; i<recordedPos; i++){
        // The ball should fly with increasing z-distance
        if(ballPath[i].z - ballPath[i - 1].z < 0)
            result = false;

        // At least one point intersection between circle and path only considering xy-plane
        cv::Point3d intersect1, intersect2;
        intersect1 = circle_line_intersection(outputCircle.maxRadius, outputCircle.centre, ballPath[i - 1], ballPath[i], true);
        intersect2 = circle_line_intersection(outputCircle.maxRadius, outputCircle.centre, ballPath[i - 1], ballPath[i], false);

        // Check the solutions are within line segments or not
        if(point_in_line_segment(intersect1, ballPath[i - 1], ballPath[i]) == true && intersect1.z != -1.0 && pointCount < 10){
            // Display on screen
            cv::line(imageDisplay, cv::Point(intersect1.x - 5, intersect1.y), cv::Point(intersect1.x + 5, intersect1.y), Scalar(255, 0, 0), 2);
            cv::line(imageDisplay, cv::Point(intersect1.x, intersect1.y - 5), cv::Point(intersect1.x, intersect1.y + 5), Scalar(255, 0, 0), 2);
            // Return only points lied within line segments
            pointsOnLine[pointCount].point1 = ballPath[i - 1];
            pointsOnLine[pointCount].point2 = ballPath[i];
            pointsOnLine[pointCount].intersect = intersect1;
            pointCount += 1;
        }
        if(point_in_line_segment(intersect2, ballPath[i - 1], ballPath[i]) == true && intersect1.z != -1.0 && pointCount < 10){
            // Display on screen
            cv::line(imageDisplay, cv::Point(intersect2.x - 5, intersect2.y), cv::Point(intersect2.x + 5, intersect2.y), Scalar(255, 0, 0), 2);
            cv::line(imageDisplay, cv::Point(intersect2.x, intersect2.y - 5), cv::Point(intersect2.x, intersect2.y + 5), Scalar(255, 0, 0), 2);
            // Return only points lied within line segments
            pointsOnLine[pointCount].point1 = ballPath[i - 1];
            pointsOnLine[pointCount].point2 = ballPath[i];
            pointsOnLine[pointCount].intersect = intersect2;
            pointCount += 1;
        }
    }

    if(pointCount >= 2 && recordedPos >= 2){
        cout << "Intersection Point 1: " << pointsOnLine[0].intersect << endl;
        cout << "Rod Position: " << zPos << endl;
        cout << "Intersection Point 2: " << pointsOnLine[pointCount - 1].intersect << endl;

        if(floor(pointsOnLine[0].intersect.z) <= zPos && zPos <= ceil(pointsOnLine[pointCount - 1].intersect.z)){
            cout << "Cause by two points to return goal" << endl;
            result = true;
        }
        else{
            cout << "Cause by two points to return fail" << endl;
        }
    }

    // Case 2: Get only one intersection points for path
    else if(pointCount <= 1 && recordedPos >= 2){
		if (ballPath[recordedPos - 1].x - ballPath[recordedPos - 2].x == 0)
			return;
        
        cv::Point3d intersect1_last_line, intersect2_last_line;
        intersect1_last_line = circle_line_intersection(outputCircle.maxRadius, outputCircle.centre, ballPath[recordedPos - 2], ballPath[recordedPos - 1], true);
        intersect2_last_line = circle_line_intersection(outputCircle.maxRadius, outputCircle.centre, ballPath[recordedPos - 2], ballPath[recordedPos - 1], false);

        cv::Point3d intersect1, intersect2;
        if (pointCount == 1)
            intersect1 = pointsOnLine[0].intersect;
        else{
            intersect1 = (intersect1_last_line.z < intersect2_last_line.z) ? intersect1_last_line : intersect2_last_line;

            cv::line(imageDisplay, cv::Point(intersect1.x - 5, intersect1.y), cv::Point(intersect1.x + 5, intersect1.y), Scalar(0, 0, 255), 2);
            cv::line(imageDisplay, cv::Point(intersect1.x, intersect1.y - 5), cv::Point(intersect1.x, intersect1.y + 5), Scalar(0, 0, 255), 2);
        }

        intersect2 = (intersect1_last_line.z > intersect2_last_line.z) ? intersect1_last_line : intersect2_last_line;

        // Draw intersections
        cv::line(imageDisplay, cv::Point(intersect2.x - 5, intersect2.y), cv::Point(intersect2.x + 5, intersect2.y), Scalar(0, 0, 255), 2);
        cv::line(imageDisplay, cv::Point(intersect2.x, intersect2.y - 5), cv::Point(intersect2.x, intersect2.y + 5), Scalar(0, 0, 255), 2);

        cout << "Intersection Point 1: " << intersect1 << endl;
        cout << "Rod Position: " << zPos << endl;
        cout << "Intersection Point 2: " << intersect2 << endl;

        if (floor(intersect1.z) <= zPos && zPos <= ceil(intersect2.z)) {
            cout << "Cause by one or zero point to return goal" << endl;
            result = true;
        }
        else{
            cout << "Cause by one or zero point to return fail" << endl;
        }
    }

    // Put text on displayed image
	if (result == true && recordedPos > 0) {
        lastUpdateTime = time(NULL);
        shootingResult = true;
		cout << "GOAL" << endl;
	}
	else if (result == false && recordedPos > 0) {
        lastUpdateTime = time(NULL);
        shootingResult = false;
		cout << "FAIL" << endl;
	}
    delete[] pointsOnLine;
}

bool point_in_circle(cv::Point centre, int radius, cv::Point test_point){
    int dx = abs(test_point.x - centre.x);
    int dy = abs(test_point.y - centre.y);

    if (dx > radius || dy > radius)
        return false;

    if (dx + dy <= radius)
        return true;

    if (pow(dx, 2) + pow(dy, 2) <= pow(radius, 2))
        return true;
    else
        return false;
}

void ballFilter(){
    cv::Mat cannyEdge, temp;
    vector<vector<cv::Point> > contours;
    vector<Vec4i> hierarchy;

	if (circleExist == false)
		return;

    // Trim out lower half of image
    for(int j=0; j<imageForBall.cols; j++){
        for(int i=0; i<imageForBall.rows; i++){
            if(j > outputCircle.centre.x - rodTrimArea && j < outputCircle.centre.x + rodTrimArea && i > outputCircle.centre.y + outputCircle.maxRadius + circleThickness)
                imageForBall.at<uchar>(i, j) = 0;
            // Assume that the ball must be higher than image centre, change here if using raw 16 bits
            if(i >= imageForBall.rows/2)
                imageForBall.at<uchar>(i, j) = 0;
            // Delete Circle
            if(!point_in_circle(outputCircle.centre, outputCircle.maxRadius, cv::Point(j, i)) && point_in_circle(outputCircle.centre, outputCircle.maxRadius + 18, cv::Point(j, i)))
                imageForBall.at<uchar>(i, j) = 0;
        }
    }
    // Function(sourceImage, destImage, params);
    medianBlur(imageForBall, temp, 2 * medianBlurValue + 1);
    Canny(temp, cannyEdge, cannyLower, cannyUpper);
    // cv::imshow("Median Blur", temp);
    findContours(cannyEdge, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    // Draw all contours with filled colour
    int maxContour = 0;
    Scalar color(255, 0, 0);
    for(int i = 0; i < contours.size(); i++){ // Iterate through each contour
		if (contourArea(contours[i]) > areaThreshold && contourArea(contours[i]) < maxBallSize) {
			drawContours(imageDisplay, contours, i, color, CV_FILLED, 8, hierarchy);
			cout << "Area: " << contourArea(contours[i]) << endl;
		}
        if(i > 0){
            if(contourArea(contours[i]) > contourArea(contours[maxContour]) && contourArea(contours[i]) < maxBallSize)
                maxContour = i;
        }
    }

    if(contours.size() > 0){
		vector<vector<Point> > contours_poly(contours.size());
		vector<Rect> boundRect(contours.size());
		vector<Point2f>center(contours.size());
		vector<float>radius(contours.size());
        // Get minimum ellipse of contours
        for(int i = 0; i < contours.size(); i++){
			if (contourArea(contours[i]) > areaThreshold && contourArea(contours[i]) < maxBallSize) {
				approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
				boundRect[i] = boundingRect(Mat(contours_poly[i]));
				minEnclosingCircle((Mat)contours_poly[i], center[i], radius[i]);
			}
        }
        
        // Draw centre on image
        // cout << "{ " << massCentre[0].x << ", " << massCentre[0].y << " }" << endl;
        // cv::line(rawImage, cv::Point(massCentre[0].x - 5, massCentre[0].y), cv::Point(massCentre[0].x + 5, massCentre[0].y), Scalar(255, 255, 0), 2);
        // cv::line(rawImage, cv::Point(massCentre[0].x, massCentre[0].y - 5), cv::Point(massCentre[0].x, massCentre[0].y + 5), Scalar(255, 255, 0), 2);

        // Record current first point to vector array
		if (center[maxContour].x > 0 && center[maxContour].y > 0 && recordedPos < 20) {
			ballPath[recordedPos].x = center[maxContour].x;
            ballPath[recordedPos].y = center[maxContour].y;
			ballPath[recordedPos].z = (int)rawImage.at<IMAGE_FORMAT>(int(center[maxContour].y), int(center[maxContour].x));
			recordedPos += 1;
			detectedBall = 1;
		}
		else if (recordedPos == 20 && center[maxContour].x > 0 && center[maxContour].y > 0) {
			recordedPos = 0;
			ballPath[recordedPos].x = center[maxContour].x;
            ballPath[recordedPos].y = center[maxContour].y;
			ballPath[recordedPos].z = (int)rawImage.at<IMAGE_FORMAT>(int(center[maxContour].y), int(center[maxContour].x));
			recordedPos += 1;
			detectedBall = 1;
		}
		else {
			detectedBall -= 1;
		}
    }
    else{
        detectedBall -= 1;
    }

    // Reset vector array if cannot detect ball in consecutive 5 frames
    // if(detectedBall == -4){
    //     recordedPos = 0;
    // }

    // Draw trace line with mutex lock to achieve mutually exclusive
    for(int i=1; i<recordedPos; i++)
        cv::line(imageDisplay, cv::Point(ballPath[i-1].x, ballPath[i-1].y), cv::Point(ballPath[i].x, ballPath[i].y), Scalar(0, 255, 255), 2);
	cannyEdge.release();
	temp.release();
}

void kinectInit(){
    libfreenect2::Freenect2Device::Config config;
    config.MinDepth = 2.0f;
    config.MaxDepth = 6.0f;

    while (freenect2.enumerateDevices() == 0 && protonect_shutdown != true)
        std::cout << "no device connected!" << std::endl;

    string serial = freenect2.getDefaultDeviceSerialNumber();


    int depthProcessor = Processor_cl;
    dev = freenect2.openDevice(serial);
    dev->setConfiguration(config);

    while (dev == 0 && protonect_shutdown != true)
        std::cout << "failure opening device!" << std::endl;

    // libfreenect2::SyncMultiFrameListener listener(libfreenect2::Frame::Depth);
    // libfreenect2::FrameMap frames;

    dev->setColorFrameListener(&listener);
    dev->setIrAndDepthFrameListener(&listener);

    dev->start();

    std::cout << "device serial: " << dev->getSerialNumber() << std::endl;
    std::cout << "device firmware: " << dev->getFirmwareVersion() << std::endl;

    registration = new libfreenect2::Registration(dev->getIrCameraParams(), dev->getColorCameraParams());

    libfreenect2::Frame undistorted(512, 424, 4), registered(512, 424, 4);
}

void imageProcessing(cv::Mat rodImage, int lowerColorRange){
    // Create thread to perform two separated tasks
    circleExist = false;
    preFiltering(rawImage, rodImage, lowerColorRange);
	ballFilter();

	if (detectedBall == -3 && recordedPos > 0) {
		cout << "Goal Detection is running" << endl;
		goalDetection();
        recordedPos = 0;
	}

    zPos = -1;
}

void initGPIO(){
    cout << "Initializing the GPIO Pins" << endl;

    goalSignal = gpio298;  // Ouput
    // Make the button and led available in user space
    gpioExport(goalSignal);
    gpioSetDirection(goalSignal, outputPin);

    gpioSetValue(goalSignal, off);
}

int main(int argc, char** argv){
	int lowerColor, key_pressed;
	
	signal(SIGINT, sigint_handler);

    if (getuid()){
        cout << "Run program with root previlege" << endl;
        return 0;
    }

	kinectInit();
    initGPIO();

	cv::namedWindow("Depth Map");

	// Pass by reference
	colorCalculation(&lowerColor);

    // Create debug folder if it does not exist
    struct stat st;
    if(stat("debug/", &st) == -1)
        mkdir("debug", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	while (!protonect_shutdown){
	    cv::Mat realImage;
	    listener.waitForNewFrame(frames);
        libfreenect2::Frame *depth = frames[libfreenect2::Frame::Depth];

        // convertTo: https://stackoverflow.com/questions/8531597/opencv-mat-cv-32fc1
        // Pixel Types: http://monkeycoding.com/?p=531
        cv::Mat(depth->height, depth->width, CV_32FC1, depth->data).copyTo(realImage);

	    // Create matrix object with same resolution of 16bit depth map
	    cv::Mat image16bit(depth->height, depth->width, CV_16UC1);

        // Scale up by multiplying 8000
        realImage.convertTo(image16bit, CV_16U);

	    // mDepthImg = 16bit unsigned, mImg8bit = 8bit unsigned
	    cv::Mat image8bit(depth->height, depth->width, CV_8UC1);

	    realImage.convertTo(image8bit, CV_8U, 255.0f / 8000.0f);

	    // Change here if using raw 16 bits
	    image16bit.copyTo(rawImage);
        image16bit.copyTo(rodImage);
	    image8bit.copyTo(imageForBall);

        // Do not change for display purpose
        image8bit.copyTo(imageDisplay);
        cvtColor(imageDisplay, imageDisplay, COLOR_GRAY2BGR);

	    // Create thread to perform two separated tasks
	    imageProcessing(rodImage, lowerColor);

	    // cv::imshow to display image
        display_on_screen();
	    cv::imshow("Depth Map", imageDisplay);

        int key = cv::waitKey(1);

        if (key == 10){
            vector<int> compression_params;
            compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
            compression_params.push_back(1);
            imwrite("debug/image" + to_string(imageNumber) + ".png", image16bit, compression_params);
            imageNumber += 1;
        }

	    // Shutdown on escape
        protonect_shutdown = protonect_shutdown || (key > 0 && ((key & 0xFF) == 27));
        listener.release(frames);

	    rawImage.release();
	    imageDisplay.release();
	    realImage.release();
	    rodImage.release();
	    imageForBall.release();
        imageDisplay.release();
        image16bit.release();
        image8bit.release();
    }
	
	dev->stop();
    dev->close();
    delete registration;
    gpioUnexport(goalSignal);

	return 0;
}
