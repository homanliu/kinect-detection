#include "jetsonGPIO/jetsonGPIO.h"

#define IMAGE_FORMAT uint16_t
// #define IMAGE_FORMAT uchar
#define Processor_cl 1

using namespace std;
using namespace cv;

cv::Mat rawImage, imageForBall, rodImage, imageDisplay;

typedef struct {
	cv::Point centre;
	int distance;
    int maxRadius;
    int depthPos;
} circleInfo;

typedef struct {
    cv::Point3i point1;
    cv::Point3i point2;
    cv::Point3d intersect;
} intersectionPoint;

libfreenect2::Freenect2 freenect2;
libfreenect2::Freenect2Device *dev = NULL;
libfreenect2::PacketPipeline  *pipeline = NULL;
libfreenect2::SyncMultiFrameListener listener(libfreenect2::Frame::Depth);
libfreenect2::FrameMap frames;
libfreenect2::Registration* registration;
bool protonect_shutdown = false;

// Here we use millimeter
const int fenceHeight = 2500;
// Kinect height may be changed
const int kinectHeight = 1300;
// Smallest value here
const int fenceToKinect = 3000;
const int tolerance = 200;
const int maxDepthRange = 8000;
const int rodLength = 35;
const int rodWidth = 7;

const int medianBlurValue = 5;
const int cannyLower = 10;
const int cannyUpper = 150;

const int maxTZ1Height = 195;

// Global results of Goal and Ball tracing
int detectedBall = 0, recordedPos = 0, zPos = -1;
cv::Point3i ballPath[20];
circleInfo outputCircle;
bool circleExist = false;

// Ball detection constants
const int areaThreshold = 0;
const int rodTrimArea = 8;
const int maxBallSize = 500;
const int circleThickness = 10;

// Display timer on screen
time_t lastUpdateTime = 0;
bool shootingResult = false;
int imageNumber = 1;

class Queue{
	public:
		Queue(int inputSize){
            // Constructor
			validElement = 0;
			circleArray = new circleInfo[inputSize];
			for(int i=0; i<inputSize; i++){
				circleArray[i].centre.x = -1;
				circleArray[i].centre.y = -1;
				circleArray[i].distance = -1;
                circleArray[i].depthPos = -1;
			}
			arraySize = inputSize;
		}
        
		void enqueue(cv::Point input, int maxRadius, int depthPos){
            // Dequeue and enqueue input element if full
			if(validElement < arraySize){
				circleArray[validElement].centre.x = input.x;
				circleArray[validElement].centre.y = input.y;
				circleArray[validElement].distance = pow(input.x, 2) + pow(input.y, 2);
                circleArray[validElement].maxRadius = maxRadius;
                circleArray[validElement].depthPos = depthPos;
				validElement += 1;
			}
			else{
				for(int i=1; i<arraySize; i++){
					circleArray[i - 1].centre.x = circleArray[i].centre.x;
					circleArray[i - 1].centre.y = circleArray[i].centre.y;
					circleArray[i - 1].distance = circleArray[i].distance;
                    circleArray[i - 1].maxRadius = circleArray[i].maxRadius;
                    circleArray[i - 1].depthPos = circleArray[i].depthPos;
				}
				circleArray[arraySize - 1].centre.x = input.x;
				circleArray[arraySize - 1].centre.y = input.y;
				circleArray[arraySize - 1].distance = pow(input.x, 2) + pow(input.y, 2);
                circleArray[arraySize - 1].maxRadius = maxRadius;
                circleArray[arraySize - 1].depthPos = depthPos;
			}
		}

		circleInfo medianDistance(){
			int index;
			if(validElement == 1)
				return circleArray[validElement - 1];

            // Sort according to centre distance with (0, 0) and radius
			int *tempDistance = new int[validElement];
			for(int i=0; i<validElement; i++)
				tempDistance[i] = circleArray[i].distance * 1000 + circleArray[i].maxRadius;
			sort(tempDistance, tempDistance + validElement);

            // Take most reasonable one to be centre
            int desiredList[(validElement / 2)], desiredValue;
            for(int i=0; i<(validElement / 2); i++)
                desiredList[i] = tempDistance[int(validElement / 4) + i];
            for(int i=0; i<(validElement / 2); i++){
                if(abs((desiredList[i] % 1000) - (desiredList[(i + 1) % (validElement / 2)] % 1000)) < 2){
                    if(desiredList[i] > desiredList[(i + 1) % (validElement / 2)])
                        desiredValue = desiredList[i];
                    else
                        desiredValue = desiredList[(i + 1) % (validElement / 2)];
                }
            }

			for(index=0; index<validElement; index++){
				if(circleArray[index].distance * 1000 + circleArray[index].maxRadius == desiredValue)
					break;
            }
			
            delete[] tempDistance;
			return circleArray[index];
		}

        void deleteAllocation(){
            delete[] circleArray;
        }

	private:
		circleInfo *circleArray;
		int validElement;
		int arraySize;
};

int arraySize = 10;
Queue *locationQueue = new Queue(arraySize);

jetsonTX2GPIONumber goalSignal;

void sigint_handler(int s);
void display_on_screen();
void colorCalculation(int *lowerColor);
int getRodCoordinates(int imageWidth, int **whitePoints, int pointCount);
int getCircleCoordinates(int imageHeight, int **whitePoints, int pointCount, int centreX);
int getCircleRadius(int **whitePoints, int pointCount, cv::Point centre);
void drawBoundingArea(cv::Mat rawImage, cv::Mat rodImage, int **whitePoints, int pointCount);
void preFiltering(cv::Mat rawImage, cv::Mat rodImage, int lowerColorRange);
cv::Point3d circle_line_intersection(int circle_radius, cv::Point circle_centre, cv::Point3i point1, cv::Point3i point2, bool return_point1);
bool point_in_line_segment(cv::Point3d point, cv::Point3i line_point1, cv::Point3i line_point2);
void goalDetection();
bool point_in_circle(cv::Point centre, int radius, cv::Point test_point);
void ballFilter();
void kinectInit();
void imageProcessing(cv::Mat rodImage, int lowerColorRange);
void initGPIO();
